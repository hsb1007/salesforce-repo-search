import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import VueTimeago from 'vue-timeago'
import vueFilterPrettyBytes from 'vue-filter-pretty-bytes'

Vue.config.productionTip = false
Vue.use(VueTimeago, {
  locale: 'en', // Default locale
})
Vue.use(vueFilterPrettyBytes)

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
