# Salesforce Repo Search

This project is created as part of coding test.

Vue JS framework is used along with Vue Router / Vuetify

All the vue related components are stored in /src folder.

Non-vue component is structured in /helper folder for easier access.

The githubHelper was created (even though it seems unnecessary for this project) in order to easily expand the scope of site feature.



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

Once it is sucessful, open a browser with url : http://localhost:8080

### Compiles and minifies for production
```
npm run build
```
