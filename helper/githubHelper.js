const { Octokit } = require("@octokit/core");
var auth = null; // @TODO to be moved to environment variable when necessary
const octokit = new Octokit({auth: auth})

module.exports.getOrgRepos = async(orgName, type="public") => {
    var response = await octokit.request("GET /orgs/{org}/repos", {
        org: orgName,
        type: type,
    });

    return response.data;
}

module.exports.getStargazerList = async(owner, repoName) => {
    var response = await octokit.request("GET /repos/{owner}/{repo}/stargazers", {
        owner: owner,
        repo: repoName,
    });

    return response.data;
}
